# This file is a part of pygments-murky. It provides a "Murky" theme for
# pygments.
#
#  Copyright (C) 2023  Tarun Prabhu <tarun.prabhu@minilx.com>
#
#  This program is free software; you can redistribute it and/or modify it under
#  the terms of the GNU General Public License as published by the Free Software
#  Foundation; either version 3 of the License, or (at your option) any later
#  version.
#
#  This program is distributed in the hope that it will be useful, but WITHOUT
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
#  FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
#  details.
#
#  You should have received a copy of the GNU General Public License along with
#  this program; if not, write to the Free Software Foundation, Inc., 59 Temple
#  Place, Suite 330, Boston, MA  02111-1307  USA
#

from pygments.style import Style
from pygments.token import \
    Comment, \
    Error, \
    Generic, \
    Keyword, \
    Name, \
    Number, \
    Operator, \
    String, \
    Token

class Murky(Style):
    """
    Pygments version of the "Murky" theme
    """

    black = '#1c1c1c'
    slate = '#3e3e3e'
    grey = '#909090'
    silver = '#c0c8cc'
    white = '#dddddd'
    red = '#e06c75'
    orange = '#d2982d'
    yellow = '#ecec17'
    green = '#97ca72'
    blue = '#7bb2dd'
    violet = '#9933ff'
    cyan = '#3ccadd'
    pink = '#e6a6be'
    brass = '#ac9f3c'

    black2 = '#101010'
    slate2 = '#282828'
    grey2 = '#707070'
    silver2 = '#a8a8a8'
    red2 = '#a8242f'
    orange2 = '#fcaf1e'
    yellow2 = '#cccc00'
    green2 = '#5e9438'
    blue2 = '#4d9dff'
    violet2 = '#8000ff'
    pink2 = '#f08ff0'

    background_color = slate2
    highlight_color = slate
    line_number_color = grey2

    styles = {
        Token: silver,
        Keyword: 'bold ' + green2,
        Number: silver,
        Error: 'bold ' + red2,
        Comment: grey,
        Comment.HashBang: 'italic ' + grey2,
        Comment.Preproc: green,
        Comment.Special: 'italic',
        Name.Builtin: green,
        Name.Variable: orange,
        Name.Constant: blue,
        Name.Class: blue,
        Name.Function: orange,
        Name.Namespace: blue,
        Name.Attribute: blue,
        Name.Decorator: blue,
        Operator: silver,
        Operator.Word: 'bold ' + green2,
        String: brass,
        String.Doc: 'noinherit italic ' + grey,
        String.Heredoc: 'italic ' + grey,
        String.Regex: 'noinherit bold ',
        Generic.Heading: 'bold ' + white,
        Generic.Subheading: 'bold ' + silver,
        Generic.Deleted: 'bold ' + red,
        Generic.Inserted: 'bold ' + blue,
        Generic.Error: 'underline ' + red,
        Generic.Emph: 'italic',
        Generic.Strong: 'bold',
        Generic.Prompt: yellow,
        Generic.Output: silver,
        Generic.Traceback: grey,
    }
