## pygments-murky

"Murky" theme for [https://pygments.org/](pygments). 

### Installing

```
    $ git clone git@gitlab.com:tarunprabhu/pygments-murky.git
    $ pip install [--user] pygments-murky
```

Use the `--user` option to install to a local directory. If the flag is not 
used, `pip` will try to install to the system directory. The "Murky" style 
should now be available for `pygmentize` to use. To check that this is the
case, run

```
    $ pygmentize -S Murky -f python
```
